# CheckFlashConfig

This is the CheckFlashConfig utility from the [Arduino core for ESP8266](https://github.com/esp8266/Arduino/) project, packaged up for easy use with PlatformIO. As it stands, I've set it up for the ESP-01S boards (ESP-01 with a 1-MB flash chip) that I have. This will tell you if your boards work, and what type and how much flash is on them. In particular, I needed the "flash ide mode" output (QIO, as it was) to pass along to the [ESP3D](https://github.com/luc-github/ESP3D/) build.

(I'm using [this ESP-01S board](https://amzn.to/3bMFkGP) and [this USB programmer](https://amzn.to/3bROqlq).  Some adjustments may be needed for other hardware.)

1. Load this project into PlatformIO.
2. Plug the ESP board into the programmer.  The ESP board should be over the programmer, like in [this image](https://alfter.us/wp-content/uploads/2021/02/IMG_20210228_202225-scaled.jpg); if it's the other way, you'll most likely let the magic smoke out.
3. Flip the switch to "program" and plug it into a USB port.
4. From PlatformIO's list of project tasks, click "Upload and Monitor."  Wait for the code to compile, upload, and start.  The monitor window should update every 5 seconds with flash size, speed, and mode...and whether the current configuration is OK.  If you're using something other than an ESP-01S, you'll probably get "flash chip configuration wrong" as one of the messages; look up your board type [here](https://docs.platformio.org/en/latest/boards/index.html#espressif-8266) and edit platformio.ini accordingly.